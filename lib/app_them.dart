import 'package:flutter/material.dart';

class AppTheme {
  static final TextStyle mainTitle = TextStyle(
    color: Colors.black45,
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );
  static final TextStyle headline = TextStyle(
    color: Colors.black54,
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );
  static final TextStyle numText = TextStyle(
    color: Colors.white,
    fontSize: 17,
    fontWeight: FontWeight.bold,
  );
  static final TextStyle smallWText = TextStyle(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static final TextStyle smallText = TextStyle(
    color: Colors.black45,
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );

  static final TextStyle smallBlueText = TextStyle(
    color: Colors.blueAccent,
    fontFamily: "Quicksand",
    fontSize: 14,
    fontWeight: FontWeight.w600,
  );
  static final TextStyle smallGrayText = TextStyle(
    color: Colors.white54,
    fontFamily: "Quicksand",
    fontSize: 15,
    fontWeight: FontWeight.w600,
  );

  static final Color backGroundColor = Colors.white;
  static final Color blackC = Colors.black87;
  static final Color textColor = Colors.black26;
}
