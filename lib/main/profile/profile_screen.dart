import 'package:flutter/material.dart';
import 'package:hamza_task/app_them.dart';
import 'package:hamza_task/main/profile/components/user_images.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "logo",
        ),
        actions: [
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Image.asset(
                "assets/images/michael-dam-12.png",
                width: 40,
                height: 40,
              ))
        ],
        leading: Image.asset(
          "assets/icons/noun_message_230466.png",
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: 414,
            decoration: BoxDecoration(
                color: Colors.black,
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(
                      "assets/images/appbar.png",
                    ))),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * .36,
                right: 22,
                left: 22),
            child: Container(
              // width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        "135",
                        style: AppTheme.numText,
                      ),
                      SizedBox(height: 4),
                      Text(
                        "post",
                        style: AppTheme.smallGrayText,
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "5,321k",
                        style: AppTheme.numText,
                      ),
                      SizedBox(height: 4),
                      Text(
                        "followers",
                        style: AppTheme.smallGrayText,
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        "99",
                        style: AppTheme.numText,
                      ),
                      Text(
                        "following",
                        style: AppTheme.smallGrayText,
                      ),
                    ],
                  ),
                  Container(
                    height: 30,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.blueAccent),
                    child: Center(
                      child: Text(
                        "Friend Request",
                        style: AppTheme.smallWText,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * .4 + 20,
            child: Container(
              // height: ,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 22, vertical: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Jenna Feranandez",
                    style: AppTheme.headline,
                  ),
                  RichText(
                    text: TextSpan(
                        text: "Creative Designer ",
                        style: AppTheme.smallText,
                        children: [
                          TextSpan(
                              text: "@ArgoRadius",
                              style: AppTheme.smallBlueText)
                        ]),
                  ),
                  SizedBox(height: 8),
                  RichText(
                    text: TextSpan(
                        text:
                            "Obsessed with Fahim MD's YouTube channel, love to go shopping on weekends and loveee food ",
                        style: AppTheme.smallText.copyWith(fontSize: 12),
                        children: [
                          TextSpan(
                              text: " #foodielife ",
                              style:
                                  AppTheme.smallBlueText.copyWith(fontSize: 12))
                        ]),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * .5,
                    child: GridView.builder(
                        shrinkWrap: false,
                        itemCount: userImages.length,
                        scrollDirection: Axis.vertical,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            // crossAxisSpacing: 5,
                            // mainAxisSpacing: 5,
                            childAspectRatio: 1.1),
                        itemBuilder: (context, index) {
                          return Image.asset(userImages[index]);
                        }),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
