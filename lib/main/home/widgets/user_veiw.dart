import 'package:flutter/material.dart';
import 'package:hamza_task/app_them.dart';
import 'package:hamza_task/db/models/user_model.dart';

class UserView extends StatelessWidget {
  UserModel? userModel;
  UserView({
    this.userModel,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Stack(
                    children: [
                      Image.asset(userModel!.profileImg!),
                      userModel!.isActive!
                          ? Positioned(
                              right: 0,
                              child:
                                  Image.asset("assets/icons/Ellipse 102.png"))
                          : Container(),
                    ],
                  ),
                  SizedBox(width: 5),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        userModel!.name!,
                        style: AppTheme.headline,
                      ),
                      SizedBox(height: 4),
                      Row(
                        children: [
                          Text(
                            userModel!.address!,
                            style: AppTheme.smallText,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 4),
                            width: 1,
                            height: 1,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: AppTheme.blackC),
                          ),
                          Text(
                            userModel!.activeFrom!,
                            style: AppTheme.smallText,
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Image.asset("assets/icons/noun_dots.png"),
              ),
            ],
          ),
          Container(
            height: 200,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                physics: BouncingScrollPhysics(),
                itemCount: userModel!.images!.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(10),
                    child: Image.asset(
                      userModel!.images![index],
                      fit: BoxFit.cover,
                    ),
                  );
                }),
          ),
        ],
      ),
    );
  }
}
