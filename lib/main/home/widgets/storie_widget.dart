import 'package:flutter/material.dart';
import 'package:hamza_task/app_them.dart';

class Storie extends StatelessWidget {
  String? name;
  String? img;
  Storie({this.img, this.name, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            children: [
              Image.asset(
                "assets/images/Ellipse 34.png",
                width: 60,
                height: 60,
              ),
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 5, color: Colors.transparent)),
                  child: Image.asset(
                    img!,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Center(
            child: Text(
              name!,
              style: AppTheme.smallText,
            ),
          )
        ],
      ),
    );
  }
}
