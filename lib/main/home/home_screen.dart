import 'package:flutter/material.dart';
import 'package:hamza_task/app_them.dart';
import 'package:hamza_task/db/models/user_model.dart';
import 'package:hamza_task/main/home/widgets/storie_widget.dart';
import 'package:hamza_task/main/home/widgets/user_veiw.dart';
import 'package:hamza_task/main/profile/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Text(
            "logo",
          ),
          actions: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProfileScreen()));
                  },
                  child: Image.asset(
                    "assets/images/michael-dam-12.png",
                  ),
                ))
          ],
          leading: Image.asset(
            "assets/icons/noun_message_230466.png",
          )),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Text(
              "Recommendations",
              style: AppTheme.headline,
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * .18,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Storie(
                  img: "assets/images/MaskGroup25.png",
                  name: "Jams Michel",
                ),
                Storie(
                  img: "assets/images/Mask Group 23.png",
                  name: " Bessie Sima...",
                ),
                Storie(
                  img: "assets/images/Mask Group 26.png",
                  name: "Jeffery Hall",
                ),
                Storie(
                  img: "assets/images/Component 8 – 1.png",
                  name: "Judy Adler",
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 8),
            height: 1,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                spreadRadius: -2,
                blurRadius: 5,
                offset: Offset.zero,
                color: Colors.black12,
              )
            ]),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            height: MediaQuery.of(context).size.height * .5,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: users.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                return UserView(
                  userModel: users[index],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
