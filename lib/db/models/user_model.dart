class UserModel {
  String? profileImg;
  String? name;
  String? address;
  String? activeFrom;
  bool? isActive;
  List<String>? images;
  UserModel({
    this.name,
    this.profileImg,
    this.address,
    this.activeFrom,
    this.isActive,
    this.images,
  });
}

List<UserModel> users = [
  UserModel(
    name: "Georgia Bates",
    profileImg: "assets/images/Component 7 – 1.png",
    address: "Toronto, Ontario",
    activeFrom: "30s ago",
    isActive: true,
    images: [
      "assets/images/sam-manns-379040-unsplash (1).png",
      "assets/images/juliana-malta-777706-unsplash.png",
      "assets/images/alp-studio-348253-unsplash.png",
      "assets/images/sam-manns-379040-unsplash (1).png",
      "assets/images/juliana-malta-777706-unsplash.png",
      "assets/images/alp-studio-348253-unsplash.png"
    ],
  ),
  UserModel(
    name: "Johny Vino",
    profileImg: "assets/images/Mask Group 31.png",
    address: "Mississauga, Ontario",
    activeFrom: "5mins ago",
    isActive: false,
    images: [
      "assets/images/stephen-arnold-114252-unsplash.png",
      "assets/images/adam-whitlock-270558-unsplash.png",
      "assets/images/warren-wong-1.png",
    ],
  ),
  UserModel(
    name: "Scott Horsfall",
    profileImg: "assets/images/Mask Group 30.png",
    address: "Markham, Ontario",
    activeFrom: "10mins ago",
    isActive: true,
    images: [
      "assets/images/adam-jaime-119551-unsplash.png",
      "assets/images/elevate-755046-unsplash.png",
      "assets/images/alex-knight-199364-unsplash.png"
    ],
  ),
  UserModel(
    name: "Meagan Ryan",
    profileImg: "assets/images/Mask Group 28.png",
    address: "Oakvill Ontario",
    activeFrom: "2days ago",
    isActive: false,
    images: [
      "assets/images/adam-jaime-119551-unsplash.png",
      "assets/images/elevate-755046-unsplash.png",
      "assets/images/alex-knight-199364-unsplash.png"
    ],
  ),
];
