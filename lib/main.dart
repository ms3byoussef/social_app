import 'package:flutter/material.dart';
import 'package:hamza_task/app_them.dart';
import 'package:hamza_task/main/home/home_screen.dart';

import 'main/profile/profile_screen.dart';

import 'package:device_preview/device_preview.dart';

void main() => runApp(
      DevicePreview(
        builder: (context) => MyApp(), // Wrap your app
      ),
    );

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: DevicePreview.appBuilder,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          accentColor: Colors.black26,
          primaryColor: Colors.white,
          appBarTheme: AppBarTheme(
            centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 0,
            shadowColor: Colors.white,
            titleTextStyle: AppTheme.mainTitle,
          )),
      home: HomeScreen(),
    );
  }
}
